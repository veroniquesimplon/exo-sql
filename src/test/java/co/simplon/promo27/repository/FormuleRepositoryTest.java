package co.simplon.promo27.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;


import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.List;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import co.simplon.promo27.entity.Formule;

public class FormuleRepositoryTest {

    @BeforeEach
    void setUp() {
        try {
            ScriptRunner runner = new ScriptRunner(Database.connect());
            runner.runScript(new InputStreamReader(getClass().getResourceAsStream("/database.sql")));
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Test
    void testFindAll() {
        FormuleRepository repo = new FormuleRepository();
        List<Formule> result = repo.findAll();

        assertEquals(1, result.get(0).getId());
        assertEquals("Menu Pizza", result.get(0).getName());
        assertEquals(15, result.get(0).getPrice());

        assertNotNull(result.get(0).getId());
        assertNotNull(result.get(0).getName());
        assertNotNull(result.get(0).getPrice());

    }

    @Test
    void findById() {
        FormuleRepository repo = new FormuleRepository();
        Formule result = repo.findById(1);

        assertEquals(1, result.getId());
        assertEquals("Menu Pizza", result.getName());
        assertEquals(20, result.getPrice());
    }
    
    @Test
    void persistSuccess() {
        FormuleRepository repo = new FormuleRepository();
        Formule nouvelleFormule = new Formule("Menu Pizza", 25);
        boolean result = repo.persist(nouvelleFormule);
        assertFalse(result);

        assertEquals(10, repo.findAll().size());
    }

    @Test
    void Update() {
        FormuleRepository repo = new FormuleRepository();
        Formule nouvelleFormule = new Formule(1,"Menu Pizza Burger", 23);
        boolean result = repo.update(nouvelleFormule);

        assertTrue(result);

    }

    @Test
    void delete() {
        FormuleRepository repo = new FormuleRepository();

        boolean result = repo.delete(1);
        assertTrue(result);

        assertEquals(9, repo.findAll().size());
    }

}
