package co.simplon.promo27.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.simplon.promo27.entity.Formule;


public class FormuleRepository {

    public List<commande> findAllCommandes() {
        List<commande> list = new ArrayList<>();
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM  commande INNER JOIN commande_formule ON commande_formule.id_commande=commande.id  WHERE commande_formule.id_formule="?"");
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                Formule formule = new Formule(
                        result.getInt("id"), // les get de commandes de commande 
                        result.getString("name"),
                        result.getInt("prix"));
                list.add(formule);
            }
        } catch (SQLException e) {
            System.out.println("Error From Repository");
            e.printStackTrace();
        }
        return list;
    }
    
    public List<Formule> findAll() {
        List<Formule> list = new ArrayList<>();
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM formule");
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                Formule formule = new Formule(
                        result.getInt("id"),
                        result.getString("name"),
                        result.getInt("prix"));
                list.add(formule);
            }
        } catch (SQLException e) {
            System.out.println("Error From Repository");
            e.printStackTrace();
        }
        return list;
    }

    public Formule findById(int id) {
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM formule WHERE id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return new Formule(
                        result.getInt("id"),
                        result.getString("nom_client"),
                        result.getInt("prix"));
            }
        } catch (SQLException e) {
            System.out.println("Error From Repository");
            e.printStackTrace();
        }
        return null;
    }

    public boolean delete(int id) {

        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM formule WHERE id=?");
            stmt.setInt(1, id);
            if (stmt.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException e) {
            System.out.println("Error from repository");
            e.printStackTrace();
        }

        return false;
    }

    public boolean persist(Formule formule) {
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement(
                    "INSERT INTO formule (nom_client, chaise) VALUES (?, ?)"/* , Statement.RETURN_GENERATED_KEYS */);
            stmt.setInt(1, formule.getId());
            stmt.setString(2, formule.getName());
            stmt.setInt(3, formule.getId());

            if (stmt.executeUpdate() == 1) {
                // Récupérer l'id auto incrémenté pour l'assigner à la formule
                ResultSet keys = stmt.getGeneratedKeys();
                keys.next();
                formule.setId(keys.getInt(1));
                return true;
            }

        } catch (SQLException e) {
            System.out.println("Error from repository");
            e.printStackTrace();
        }
        return false;
    }

    public boolean update(Formule formule) {
        try (Connection connection = Database.connect()) {
            PreparedStatement stmt = connection.prepareStatement("UPDATE formule SET name=?, chaise=?   WHERE id=?;");
            stmt.setInt(1, formule.getId());
            stmt.setString(2, formule.getName());
            stmt.setInt(3, formule.getId());

            if (stmt.executeUpdate() == 1) {
                return true;
            }

        } catch (SQLException e) {
            System.out.println("Error from repository");
            e.printStackTrace();
        }
        return false;
    }

}
