package co.simplon.promo27.entity;

public class Ingredients {
    int id;
    String name;
    int stock;

    public Ingredients(String name, int stock) {
        this.name = name;
        this.stock = stock;
    }

    public Ingredients(int id, String name, int stock) {
        this.id = id;
        this.name = name;
        this.stock = stock;
    }

    public Ingredients(String name) {
        this.name = name;
    }

    public Ingredients(int id) {
        this.id = id;
    }

    public Ingredients(int id, String name) {
        this.id = id;
        this.name = name;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

}
