package co.simplon.promo27.entity;

public class Plat {
    int id;
    String name;
    int prix;
    String description;
    
    public Plat(int id, String name, int prix, String description) {
        this.id = id;
        this.name = name;
        this.prix = prix;
        this.description = description;
    }
    public Plat(String name, int prix, String description) {
        this.name = name;
        this.prix = prix;
        this.description = description;
    }
    public Plat(String name) {
        this.name = name;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getPrix() {
        return prix;
    }
    public void setPrix(int prix) {
        this.prix = prix;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    

}



