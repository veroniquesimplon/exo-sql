-- Active: 1709546041278@@127.0.0.1@3306@resto
DROP TABLE IF EXISTS commande_formule;
DROP TABLE IF EXISTS ingredient_plat;
DROP TABLE IF EXISTS plat_commande;
DROP TABLE IF EXISTS formule_plat;
DROP TABLE IF EXISTS formule;
DROP TABLE IF EXISTS achat;
DROP TABLE IF EXISTS commande;
DROP TABLE IF EXISTS tablee;
DROP TABLE IF EXISTS plat;
DROP TABLE IF EXISTS ingredient;



CREATE TABLE ingredient(
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(60) NOT NULL,
    stock INT
    );

CREATE TABLE plat(
        id INT PRIMARY KEY AUTO_INCREMENT,
        name VARCHAR(90) NOT NULL,
        prix INT NOT NULL
    );

CREATE TABLE tablee(
    id INT PRIMARY KEY AUTO_INCREMENT,
    date DATETIME NOT NULL,
    nom_client VARCHAR(60) NOT NULL,
    chaise INT
);
CREATE TABLE commande(
    id INT PRIMARY KEY AUTO_INCREMENT,
    nomClient VARCHAR(60),
    total INT,
    id_tablee INT,
    Foreign Key (id_tablee) REFERENCES tablee(id)
);

CREATE TABLE formule(
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(30),
    prix INT
);

CREATE TABLE achat(
    id INT PRIMARY KEY AUTO_INCREMENT,
    nom_acheteur VARCHAR(90),
    prix INT,
    id_ingredient INT,
    Foreign Key (id_ingredient) REFERENCES ingredient(id)
);


/*TABLE DE JOINTURE*/

CREATE TABLE commande_formule(
    id_commande INT,
    id_formule INT,
    PRIMARY KEY (id_commande, id_formule),
    Foreign Key (id_commande) REFERENCES commande(id) ON DELETE CASCADE,
    Foreign Key (id_formule) REFERENCES formule(id) ON DELETE CASCADE
);

CREATE TABLE formule_plat(
    id_formule INT ,
    id_plat INT NOT NULL,
    PRIMARY KEY (id_formule, id_plat),
    Foreign Key (id_formule) REFERENCES formule(id) ON DELETE CASCADE,
    Foreign Key (id_plat) REFERENCES plat(id) ON DELETE CASCADE
);

CREATE TABLE plat_commande(
    id_plat INT,
    id_commande INT,
    PRIMARY KEY (id_plat, id_commande),
    Foreign Key (id_plat) REFERENCES plat(id) ON DELETE CASCADE,
    Foreign Key (id_commande) REFERENCES commande(id) ON DELETE CASCADE
);

CREATE TABLE ingredient_plat(
    id_ingredient INT,
    id_plat INT NOT NULL,
    PRIMARY KEY (id_ingredient, id_plat),
    Foreign Key (id_ingredient) REFERENCES ingredient(id) ON DELETE CASCADE,
    Foreign Key (id_plat) REFERENCES plat(id) ON DELETE CASCADE
);



INSERT INTO ingredient(name, stock) VALUES 
("pomme de terre", 15),     
("tomate", 5),              
("salade verte" , 10),                             
("bettrave", 13),           
("mais", 9),                
("huile d'olive", 90),      
("vinaigre", 13),           
("cerise", 3),              
("ananas", 30),             
("banane", 30),             
("fruit de la passion", 30),
("fraise", 25),             
("pain complet", 20),       
("viande d'agneau", 23),    
("viande haché", 30),       
("poulet", 30),             
("philadelphia", 50),       
("oeuf", 60),                
("sel", 30),                
("sucre", 20),              
("farine", 25),             
("citron", 20),             
("olive verte", 25),        
("pruneau", 10),            
("cheddar", 14),            
("riz", 35),                
("crevette", 25),           
("café", 10),               
("cacao en poudre", 6),     
("mascarpone", 10),         
("levure", 5),              
("fromage", 10),            
("biscuit au beurre", 25),  
("oignon", 24),             
("creme liquide", 20),      
("vanille", 20),            
("beurre", 10),             
("biscuit cuillere", 10);


INSERT INTO plat(name, prix) VALUES
("poulet aux olive", 20),           
("viande aux pruneaux", 25),       
("burger poulet", 10),             
("cheesburger", 10),               
("riz aux crevettes", 15),         
("cheescake fraise", 5),           
("cheescake ananas", 5),           
("flan", 4.5),                     
("tarte aux citron", 6.9),         
("tarte aux fraise", 6),          
("tiramisu", 5.5),                 
("smoothie", 3),                  
("pizza", 15),                  
("salade composé", 8.5);         


INSERT INTO ingredient_plat(id_ingredient, id_plat) VALUES  
(16, 1),
(23, 1),
(22, 1),
(14 ,2),
(24, 2),
(34, 2),
(3, 3),
(2, 3),
(16, 3),
(25, 3),
(34, 3),
(13, 3),
(3, 4),
(2, 4),
(14, 4),
(15, 4),
(25, 4),
(26 ,5),
(27, 5),
(35, 5),
(33, 6),
(20, 6),
(17, 6),
(12, 6),
(33, 7),
(20, 7),
(17, 7),
(9, 7),
(18 ,8),
(19, 8),
(20, 8),
(21, 8),
(36, 8),
(12, 9),
(18, 9),
(19, 9),
(20, 9),
(21, 9),
(36, 9),
(37, 9),
(22, 9),
(18, 10),
(19, 10),
(20, 10),
(21, 10),
(36, 10),
(37, 10),
(12, 10),
(18, 11),
(20, 11),
(28, 11),
(29, 11),
(30, 11),
(38, 11),
(8, 12),
(9, 12),
(10, 12),
(11, 12),
(20, 12),
(2, 13),
(6, 13),
(19, 13),
(21, 13),
(23, 13),
(31, 13),
(32, 13),
(1, 14),
(2, 14),
(3, 14),
(4, 14),
(5, 14),
(6, 14),
(7, 14),
(19, 14);

INSERT INTO tablee (date, nom_client, chaise) VALUES 
('2024-03-10 19:00:00', 'Jean Dupont', 4),
('2024-03-10 20:00:00', 'Marie Durand', 2),
('2024-03-11 18:30:00', 'Lucas Martin', 5),
('2024-03-11 19:45:00', 'Emma Petit', 3),
('2024-03-12 18:00:00', 'Hugo Lemoine', 2),
('2024-03-12 20:30:00', 'Inès Bernard', 4),
('2024-03-13 19:00:00', 'Jules Robert', 6),
('2024-03-13 21:00:00', 'Louise Dubois', 2),
('2024-03-14 19:30:00', 'Gabriel Moreau', 3),
('2024-03-14 18:15:00', 'Alice Rousseau', 4);


INSERT INTO commande (nomClient, total, id_tablee) VALUES 
('Jean Dupont', 40, 1),
('Marie Durand', 22, 2),
('Lucas Martin', 57, 3),
('Emma Petit', 34, 4),
('Hugo Lemoine', 20, 5),
('Inès Bernard', 48, 6),
('Jules Robert', 66, 7),
('Louise Dubois', 24, 8),
('Gabriel Moreau', 39, 9),
('Alice Rousseau', 42, 10);

INSERT INTO formule (name, prix) VALUES 
('Menu Pizza', 15),
('Menu Burger', 14),
('Menu Végétarien', 12),
('Menu Enfant', 10),
('Menu Poisson', 18),
('Menu Poulet', 16),
('Menu Italien', 17),
('Menu Salade', 13),
('Menu Sushi', 20),
('Menu Découverte', 22);

SELECT * FROM formule WHERE id = 1;


INSERT INTO achat (nom_acheteur, prix, id_ingredient) VALUES 
('Kevin', 30, 1),
('Claude', 45, 2),
('Marie', 25, 3),
('Marie', 10, 4),
('Marie', 40, 5),
('Claude', 15, 6),
('Kevin', 20, 7),
('Kevin', 22, 8),
('Marie', 55, 9),
('Claude', 18, 10);


INSERT INTO commande_formule (id_commande, id_formule) VALUES 
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 5),
(6, 6),
(7, 7),
(8, 8),
(9, 9),
(10, 10);

INSERT INTO formule_plat (id_formule, id_plat) VALUES 
(1, 1),
(2, 4),
(3, 7),
(4, 8),
(5, 10),
(6, 9),
(7, 5),
(8, 3),
(9, 10),
(10, 6);

INSERT INTO plat_commande (id_plat, id_commande) VALUES 
(1, 1),
(4, 2),
(7, 3),
(8, 4),
(10, 5),
(9, 6),
(5, 7),
(3, 8),
(10, 9),
(6, 10);
